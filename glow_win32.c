/* Copyright (C) 2017-2022 Alaskan Emily, Transnat Games
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "glow.h"

#include <Windows.h>
#include <gl/gl.h>

#include <assert.h>
#include <stdlib.h>

/******************************************************************************/

#define GLOW_WINDOW_STYLE\
    (WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX)

/******************************************************************************/

struct Glow_Context{
    HDC dc;
    HGLRC ctx;
    unsigned gl_mag, gl_min;
};

/******************************************************************************/

struct Glow_Window {
    HDC dc;
    HWND win;
    struct Glow_Context ctx;
    BOOL vsync, adaptive_vsync;
};

/******************************************************************************/

#ifdef __GNUC__

#ifdef WinMain
#undef WinMain
#endif

#endif

/******************************************************************************/

#define GLOW_CLASS_NAME "GlowWindow"

/******************************************************************************/

static HINSTANCE glow_app = NULL;

/******************************************************************************/

#ifdef __CYGWIN__

#ifndef SM_CXPADDEDBORDER
#define SM_CXPADDEDBORDER 92
#endif

#endif

/******************************************************************************/

static void glow_translate_local_mouse_pos(const POINT *in_pnt,
    struct Glow_Window *w, glow_pixel_coords_t out_pos){
    RECT rect;
    POINT pnt;
    pnt.x = in_pnt->x - 4;
    pnt.y = in_pnt->y - (GetSystemMetrics(SM_CYFRAME) +
        GetSystemMetrics(SM_CYCAPTION) +
        GetSystemMetrics(SM_CXPADDEDBORDER));
    
    GetWindowRect(w->win, &rect);
    
    out_pos[0] = (unsigned short)(pnt.x - rect.left);
    out_pos[1] = (unsigned short)(pnt.y - rect.top);
    
    if(!PtInRect(&rect, pnt)){
        if(pnt.x < rect.left)
            out_pos[0] = 0;
        else if(pnt.x > rect.right)
            out_pos[0] = (unsigned short)(rect.right - rect.left);
        
        if(pnt.y < rect.top)
            out_pos[1] = 0;
        else if(pnt.y > rect.bottom)
            out_pos[1] = (unsigned short)(rect.bottom - rect.top);
    }
}

/******************************************************************************/

static char glow_get_key_char(unsigned in){
    if(in <= 0x5A && in >= 0x41)
        return (in - 0x41) + 'a';
    if(in <= 0x39 && in >= 0x30)
        return (in - 0x30) + '0';
    if(in == VK_SPACE)
        return ' ';
    if(in == VK_OEM_2)
        return '/';
    if(in == VK_OEM_3)
        return '~';
    if(in == VK_OEM_4)
        return '[';
    if(in == VK_OEM_5 || in == VK_OEM_102)
        return '\\';
    if(in == VK_OEM_6)
        return ']';
    if(in == VK_OEM_7)
        return '\'';
    
    return '\0';
}

/*****************************************************************************/

static const char *glow_get_key_string(unsigned in, unsigned *len){
#define GLOW_IN_VK(N, VAL) case N: len[0] = sizeof(VAL) - 1; assert(sizeof(VAL) < 16); return VAL
#define GLOW_OEM_VK(N) case VK_OEM_ ## N: len[0] = sizeof("OEM_" #N ) - 1; return "OEM_" #N
    switch(in){
        GLOW_IN_VK(VK_ESCAPE, GLOW_ESCAPE);
        case VK_LSHIFT: case VK_RSHIFT:
        GLOW_IN_VK(VK_SHIFT, GLOW_SHIFT);
        case VK_LCONTROL: case VK_RCONTROL:
        GLOW_IN_VK(VK_CONTROL, GLOW_CONTROL);
        GLOW_IN_VK(VK_BACK, GLOW_BACKSPACE);
        GLOW_IN_VK(VK_RETURN, GLOW_ENTER);
        GLOW_IN_VK(VK_TAB, GLOW_TAB);
        GLOW_IN_VK(VK_CLEAR, "clear");
        GLOW_IN_VK(VK_LEFT, GLOW_LEFT_ARROW);
        GLOW_IN_VK(VK_DELETE, GLOW_DELETE);
        GLOW_IN_VK(VK_UP, GLOW_UP_ARROW);
        GLOW_IN_VK(VK_DOWN, GLOW_DOWN_ARROW);
        GLOW_IN_VK(VK_RIGHT, GLOW_RIGHT_ARROW);
        GLOW_OEM_VK(1);
        GLOW_OEM_VK(2);
        GLOW_OEM_VK(3);
        GLOW_OEM_VK(4);
        GLOW_OEM_VK(5);
        GLOW_OEM_VK(6);
        GLOW_OEM_VK(7);
        GLOW_OEM_VK(8);
        GLOW_OEM_VK(102);
        GLOW_IN_VK(VK_NONAME, "NONAME");
#undef GLOW_IN_VK
        default: return NULL;
    }
}

/*****************************************************************************/

static const PIXELFORMATDESCRIPTOR glow_pixel_format = {
    sizeof(PIXELFORMATDESCRIPTOR),
    1, /* Version, always 1 */
    PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,
    PFD_TYPE_RGBA,
    32, /* Color Depth */
    0, 0, 0, 0, 0, 0, /* Individual color depths and shifts */
    0,
    0,
    0,
    0, 0, 0, 0,
    16, /* Depth buffer size */
    8, /* Stencil buffer size */
    0,
    PFD_MAIN_PLANE,
    0,
    0, 0, 0
};

/******************************************************************************/

static LRESULT WINAPI glow_window_proc(HWND wnd,
    UINT msg,
    WPARAM parm,
    LPARAM lparam){
    
    switch(msg){
        case WM_CREATE:
            {
                struct Glow_Window *const window =
                    (void*)(((CREATESTRUCT*)lparam)->lpCreateParams);
                const int pix_format = ChoosePixelFormat(
                    (window->dc = GetDC(wnd)),
                    &glow_pixel_format);
                
                /* This will get set to -1 if adaptive vsync works. */
                int interval = 1;
                const char *const ext = (void*)glGetString(GL_EXTENSIONS);
                
                SetPixelFormat(window->dc, pix_format, &glow_pixel_format);
                window->ctx.ctx = wglCreateContext(window->dc);
                window->ctx.dc = window->dc;
                wglMakeCurrent(window->dc, window->ctx.ctx);
                
                /* Try to get VSync or, if requested, adaptive VSync. */
                if((window->adaptive_vsync || window->vsync) &&
                    strstr(ext, "WGL_EXT_swap_control") != NULL){
                    
                    typedef BOOL(*wgl_swap_interval_func)(int);
                    
                    const wgl_swap_interval_func wglSwapIntervalEXT =
                        (wgl_swap_interval_func)wglGetProcAddress(
                            "wglSwapIntervalEXT");
                    
                    assert(wglSwapIntervalEXT);
                    
                    /* Try to use control_tear if requested. */
                    if(window->adaptive_vsync &&
                        strstr(ext, "WGL_EXT_extensions_string") != NULL){
                        
                        typedef const char*(*wgl_extension_strings_func)(void);
                        
                        const wgl_extension_strings_func
                            wglGetExtensionsStringEXT =
                            (wgl_extension_strings_func)wglGetProcAddress(
                                "wglGetExtensionsStringEXT");
                        
                        assert(wglGetExtensionsStringEXT);
                        
                        const char *const wgl_ext =
                            wglGetExtensionsStringEXT();
                        
                        if(strstr(wgl_ext, "WGL_EXT_swap_control_tear") !=
                            NULL){
                            
                            interval = -1;
                        }
                    }
                    
                    /* Set the interval if either adaptive VSync or regular
                     * VSync was requested.
                     */
                    if(((window->adaptive_vsync && interval == -1) ||
                        window->vsync) &&
                        wglSwapIntervalEXT != NULL){
                        
                        wglSwapIntervalEXT(interval);
                    }
                }
                
                glClearColor(0.75f, 0.333f, 0.0f, 1.0f);
                SetFocus(wnd);
                return 0;
            }
        case WM_SHOWWINDOW:
            if(parm == FALSE){
                DestroyWindow(wnd);
                return 0;
            }
            else{
                ShowWindow(wnd, SW_SHOWNORMAL);
                SetFocus(wnd);
            }
            return 0;
        case WM_CLOSE:
            return DefWindowProc(wnd, msg, parm, lparam);
        case WM_NCDESTROY: /* FALLTHROUGH */
        case WM_DESTROY:
            PostQuitMessage(EXIT_SUCCESS);
            return 0;
        default:
            return DefWindowProc(wnd, msg, parm, lparam);
    }
}

/******************************************************************************/

BOOL WINAPI DllMain(HINSTANCE app, DWORD reason, LPVOID reserved){
    const HICON icon = LoadIcon(app, MAKEINTRESOURCE(101));
    const HANDLE cursor = LoadCursor(NULL, IDC_ARROW);
    WNDCLASS wc = {
        CS_OWNDC,
        glow_window_proc,
        0,
        0,
        0,
        icon,
        cursor,
        (HBRUSH)(COLOR_BACKGROUND),
        NULL,
        GLOW_CLASS_NAME
    };
    
    wc.hInstance = glow_app = app;
    
    RegisterClass(&wc);
    
    return TRUE;
}

/******************************************************************************/

void Glow_ViewportSize(unsigned w, unsigned h,
    unsigned *out_w, unsigned *out_h){
    
    out_w[0] = w;
    out_h[0] = h;
    
    return;
    
    RECT size;
    
    size.left = 0;
    size.top = 0;
    size.right = w;
    size.bottom = h;
    
    AdjustWindowRect(&size, GLOW_WINDOW_STYLE, FALSE);

    size.bottom += (GetSystemMetrics(SM_CYFRAME) +
        GetSystemMetrics(SM_CYCAPTION) +
        GetSystemMetrics(SM_CXPADDEDBORDER));
    
    {
        const DWORD w_thickness = (size.right - w), h_thickness = (size.bottom - h);
        
        out_w[0] = w + w_thickness;
        out_h[0] = h + h_thickness;
    }
}

/******************************************************************************/

void *Glow_GetProcAddress(const char *name){
    return (void*)wglGetProcAddress(name);
}

/******************************************************************************/

int Glow_CreateWindow(struct Glow_Window *out,
    unsigned w, unsigned h, const char *title, int flags){
    
    RECT size;
    /* Set the style based on the flags. */
    const DWORD style =
        (((flags & GLOW_UNDECORATED) == 0) ?
            GLOW_WINDOW_STYLE : WS_OVERLAPPED) |
        (((flags & GLOW_RESIZABLE) == 0) ?
            0 : (WS_THICKFRAME | WS_MAXIMIZEBOX));
    DWORD adj_w, adj_h;
    
    if(glow_app == NULL){
        const HINSTANCE app = glow_app = GetModuleHandle(NULL);
        const HICON icon = LoadIcon(app, MAKEINTRESOURCE(101));
        const HANDLE cursor = LoadCursor(NULL, IDC_ARROW);
        WNDCLASS wc = {
            CS_OWNDC,
            glow_window_proc,
            0,
            0,
            0,
            icon,
            cursor,
            (HBRUSH)(COLOR_BACKGROUND),
            NULL,
            GLOW_CLASS_NAME
        };
        
        RegisterClass(&wc);
    }

    size.left = 0;
    size.top = 0;
    size.right = w;
    size.bottom = h;
    /* Note that AdjustWindowRect will center the client area around the
     * left/top of the input, so we will need to use the differences for
     * the actual window dimensions. */
    if(AdjustWindowRect(&size, style, TRUE)){
        adj_w = size.right - size.left;
        adj_h = size.bottom - size.top;
    }
    else{
        adj_w = w;
        adj_w = h;
    }
    out->win = CreateWindow(GLOW_CLASS_NAME,
        title,
        style,
        64, 64, w, h,
        NULL, NULL,
        glow_app,
        out);
    if(out->win == NULL)
        return -1;
    out->adaptive_vsync = (flags & GLOW_ADAPTIVE_VSYNC) != 0;
    out->vsync = (flags & GLOW_VSYNC) != 0;
    return 0;
}

/******************************************************************************/

unsigned Glow_WindowStructSize(){
    return sizeof(struct Glow_Window);
}

/******************************************************************************/

void Glow_DestroyWindow(struct Glow_Window *w){
    wglDeleteContext(w->ctx.ctx);
    DestroyWindow(w->win);
}

/******************************************************************************/

void Glow_SetTitle(struct Glow_Window *w, const char *title){
    SetWindowText(w->win, title);
}

/******************************************************************************/

void Glow_ShowWindow(struct Glow_Window *w){
    ShowWindow(w->win, SW_SHOWNORMAL);
}

/******************************************************************************/

void Glow_HideWindow(struct Glow_Window *w){
    ShowWindowAsync(w->win, SW_HIDE);
}

/******************************************************************************/

void Glow_FlipScreen(struct Glow_Window *w){
    wglMakeCurrent(w->dc, w->ctx.ctx);
    glFinish();
    wglSwapLayerBuffers(w->dc, WGL_SWAP_MAIN_PLANE);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
}

/******************************************************************************/

void Glow_GetWindowSize(const struct Glow_Window *window,
    unsigned *out_w, unsigned *out_h){
    RECT rect;
    GetWindowRect(window->win, &rect);
    *out_w = rect.right - rect.left;
    *out_h = rect.bottom - rect.top;
}

/******************************************************************************/

GLOW_PURE static enum Glow_MouseButton glow_message_button(const MSG *msg){
	switch(msg->message){
		default:
			assert(NULL == "Invalid message for glow_message_button");
			/* FALLTHROUGH */
        case WM_LBUTTONDOWN: 
        case WM_LBUTTONUP:
			return eGlowLeft;
        case WM_RBUTTONDOWN:
        case WM_RBUTTONUP:
			return eGlowRight;
        case WM_MBUTTONDOWN:
        case WM_MBUTTONUP:
			return eGlowMiddle;
	}
}

/******************************************************************************/

static BOOL glow_translate_event(const MSG *msg, struct Glow_Window *window,
    struct Glow_Event *out_event){
    BOOL pressed = FALSE;
    switch(msg->message){
        case WM_KEYDOWN:
            pressed = TRUE;
            /* FALLTHROUGH */
        case WM_KEYUP:
            out_event->type = pressed ?
                eGlowKeyboardPressed : eGlowKeyboardReleased;
            
            TranslateMessage(msg);
            
            {
                const char c = glow_get_key_char(msg->wParam),
                    *c_str;
                unsigned len;
                if(c){
                    out_event->value.key[0] = c;
                    out_event->value.key[1] = '\0';
                    return TRUE;
                }
                else if ((c_str = glow_get_key_string(msg->wParam, &len))){
                    assert(len + 1 < GLOW_MAX_KEY_NAME_SIZE);
                    memcpy(out_event->value.key, c_str, len);
                    out_event->value.key[len] = '\0';
                    return TRUE;
                }
                else /* Drop input */
                    return FALSE;
            }
        case WM_LBUTTONDOWN: 
        case WM_RBUTTONDOWN:
        case WM_MBUTTONDOWN:
            pressed = TRUE;
			/* FALLTHROUGH */
        case WM_RBUTTONUP:
        case WM_LBUTTONUP:
        case WM_MBUTTONUP:
            glow_translate_local_mouse_pos(&msg->pt,
                window, out_event->value.mouse.xy);
            
			/* Select the button */
            out_event->value.mouse.button = glow_message_button(msg);
			
            out_event->type = pressed ?
                eGlowMousePressed : eGlowMouseReleased;
            return TRUE;
		case WM_MOUSEMOVE:
			glow_translate_local_mouse_pos(&msg->pt,
				window, out_event->value.mouse.xy);
            out_event->type = eGlowMouseMoved;
            return TRUE;
        
        case WM_DESTROY: /* FALLTHROUGH */
        case WM_CLOSE: /* FALLTHROUGH */
        case WM_QUIT: /* Note that quit actually never happens right now... */
            out_event->type = eGlowQuit;
            return TRUE;
        default:
            return FALSE;
    }
}

/******************************************************************************/

unsigned Glow_GetEvent(struct Glow_Window *window,
    struct Glow_Event *out_event){
    MSG msg;
    
    do{
        if(!PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)){
            return 0;
        }
        
        DispatchMessage(&msg);
    }while(!glow_translate_event(&msg, window, out_event));

    return 1;
}

/******************************************************************************/

void Glow_WaitEvent(struct Glow_Window *window,
    struct Glow_Event *out_event){
    MSG msg;
    
    do{
        if(GetMessage(&msg, NULL, 0, 0)){
            DispatchMessage(&msg);
        }
        else{
            /* Got a WM_QUIT */
            out_event->type = eGlowQuit;
            PostQuitMessage(msg.wParam);
            return;
        }
    }while(!glow_translate_event(&msg, window, out_event));
}

/******************************************************************************/

unsigned Glow_ContextStructSize(){
    return sizeof(struct Glow_Context);
}

/******************************************************************************/

struct Glow_Context *Glow_GetContext(
    struct Glow_Window *window){
    return &window->ctx;
}

/******************************************************************************/

void Glow_MakeCurrent(struct Glow_Context *ctx){
    wglMakeCurrent(ctx->dc, ctx->ctx);
}

/******************************************************************************/

int Glow_CreateContext(struct Glow_Window *window,
    struct Glow_Context *opt_share,
    unsigned major, unsigned minor,
    struct Glow_Context *out){

    (void)opt_share;
    (void)major;
    (void)minor;
    (void)out;

    /* TODO!!! */
    out->dc = window->ctx.dc;
    out->ctx = window->ctx.ctx;
    return 0;
}

/******************************************************************************/

void Glow_CreateLegacyContext(struct Glow_Window *window,
    struct Glow_Context *out){
    out->dc = window->ctx.dc;
    out->ctx = window->ctx.ctx;
}

/******************************************************************************/

struct Glow_Window *Glow_CreateLegacyWindow(unsigned w, unsigned h,
    const char *title) {

    /* Put the window and CTX in one location so that free() will get them both. */
    struct Glow_Window *const window =
        malloc(Glow_WindowStructSize() + Glow_ContextStructSize());
    struct Glow_Context *const ctx = (struct Glow_Context *)(window + 1);
    Glow_CreateWindow(window, w, h, title, 0);
    Glow_CreateContext(window, NULL, 2, 1, ctx);
    Glow_MakeCurrent(ctx);
    return window;
}
